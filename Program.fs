﻿
let printWithDelayAsync (s:string)(d:int) = async {
    printfn "%s Starting async to print %s after %d" (System.DateTime.Now.TimeOfDay.ToString())s d
    do! Async.Sleep d
    printfn "%s after delay %d: %s" (System.DateTime.Now.TimeOfDay.ToString()) d s
}

let printWithDelayTask (s:string)(d:int) = task {
    printfn "%s Starting task to print %s after %d" (System.DateTime.Now.TimeOfDay.ToString())s d
    do! Async.Sleep d
    printfn "%s after delay %d: %s" (System.DateTime.Now.TimeOfDay.ToString()) d s
}
// For more information see https://aka.ms/fsharp-console-apps
printfn "Hello from F#"

async {
    printfn "## Async"
    // do! blocks and so the second printWithDelayAsync is not started before the first ends
    printfn "calls with do!: starts the async and blocks does not progress until it completes"
    do! printWithDelayAsync "test1" 2000
    do! printWithDelayAsync "test2" 1000

    // asyncs are run in parallel
    printfn "calls with Async.parallel"
    printfn "assigning asyncs to variables"
    let a1 =  printWithDelayAsync "test1" 2000
    let a2 =  printWithDelayAsync "test2" 1000
    printfn "Will now run in parallel. Note that 'Starting async to print...' was not yet displayed"
    printfn "This is because asyncs are not started immediately"
    do! (Async.Parallel [|a1;a2|] |> Async.Ignore)
    do! Async.Sleep 3000

    printfn "Running Async.Sequential with the same asyncs"
    do! (Async.Sequential [|a1;a2|] |> Async.Ignore)
    do! Async.Sleep 3000

    printfn "Async cans be started on the thread pool similarly to a task"
    async {
        do! printWithDelayAsync "test1 in started async" 2000
    } |> Async.Start
    async {
        do! printWithDelayAsync "test2 in started async" 1000
    } |> Async.Start
    do! Async.Sleep 3000


    printfn "We can also assign the promise to variables"
    let promise1 = printWithDelayAsync "test1" 2000
    let promise2 = printWithDelayAsync "test2" 1000
    printfn "Promises were assigned, will now wait on them with `let! () = myPromise`"
    printfn "But before sleeping 1.5s to show the promises don't start right away."
    do! Async.Sleep 1500
    printfn "Done sleeping, will now wait on promises"
    let! () = promise1
    let! () = promise2
    printfn "Finished waiting promises"

    printfn "We can also assign the promise to variables"
    let promise1 = printWithDelayAsync "test1" 2000
    let promise2 = printWithDelayAsync "test2" 1000
    printfn "Promises were assigned, will now start them without waiting on them with `myPromise|> Async.StartAsTask`"
    printfn "Note that Async.Start can only be used with unit returning functions and makes it impossible to wait for the async to complete as it returns ()"
    printfn "StartAsTask on the other hand returns a task, on which it is possible to wait"
    let task1 = promise1 |> Async.StartAsTask
    let task2 = promise2 |> Async.StartAsTask
    printfn "Sleeping 1.5s to show the promises started right away when calling Async.StartAsTask."
    do! Async.Sleep 1500
    printfn "Done sleeping, we need to wait on the task with `do! task1 |> Async.AwaitTask`"
    printfn "Note we should not execute `do! promise1` as that would start a new run of it!"
    do! task1|> Async.AwaitTask
    do! task2|> Async.AwaitTask
    printfn "Finished waiting"

    // tasks
    printfn "## Tasks"
    printfn "Just assigning the task to a variable"
    let t1 = printWithDelayTask "test1" 2000
    printfn "t1 assigned (Notice the task is started immediately, as its first print call was already executed)"
    let t2 = printWithDelayTask "test2" 1000
    printfn "t2 assigned (Notice the task is started immediately, as its first print call was already executed)"
    printfn "awaiting tasks gives an async we need to wait for with do!"
    let a1 = t1|>Async.AwaitTask
    let a2 = t2 |> Async.AwaitTask
    printfn "But as tasks are started immediately, the do! only waits for it to complete"
    printfn "This means even though we first do! the tasks with longest delay, the other will print first"
    do! a1
    do! a2

} |> Async.RunSynchronously